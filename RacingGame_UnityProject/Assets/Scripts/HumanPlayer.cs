﻿using AiSolution.Neural_Networks;
using AiSolution.Players;
using UnityEngine;

public class HumanPlayer : IPlayer
{
    private readonly Trainer _trainer;

    public HumanPlayer()
    {
        _trainer = new Trainer();
    }

    public Move NextMove(Perception perception)
    {
        var result = new Move
        {
            Horizontal = Input.GetAxis("Horizontal"),
            Vertical = Input.GetAxis("Vertical")
        };
        //StoreTrainingExample(perception, result);
        return result;
    }

    private void StoreTrainingExample(Perception perception, Move result)
    {
        _trainer.StoreTrainingExample("dan.txt", new TrainingExample
        {
            Perception = perception,
            ExpectedMove = new Move
            {
                Horizontal = ScaleDown(result.Horizontal),
                Vertical = result.Vertical
            }
        });
    }

    private double ScaleDown(double value)
    {
        return (value + 1) / 2;
    }

    public void Dispose()
    {
        _trainer.Dispose();
    }
}