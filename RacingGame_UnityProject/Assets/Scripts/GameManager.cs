﻿using System;
using System.Collections.Generic;
using System.IO;
using AiSolution.Genetics;
using AiSolution.NeuroEvolution;
using CarPhysics;
using Configs;
using Raycasting;
using UnityEngine;
using Newtonsoft.Json;

public class GameManager : MonoBehaviour
{
    [SerializeField] private VehicleSimulatorConfig _config;
    [SerializeField] private VehicleSpawnerConfig _spawnerConfig;
    [SerializeField] private MovementManager _movementManager;
    [SerializeField] private TextAsset _neuroConfigJson;

    public TrackFactory Factory;

    private TrackModel _trackModel;
    private Vehicle _vehicle;

    private void Start()
    {
        _trackModel = new TrackModel(Factory.TrackData, 4);
        var trackCollision = new TrackCollision(_trackModel);

        _movementManager.Inject(_trackModel);


        var neuroConfig = NeuroEvolutionConfigChooser(trackCollision);

        var vehicleView = Instantiate(_spawnerConfig.VehiclePrefab);
        
        var carSimulator = new CarSimulator(_config.Mass, _config.Friction, _config.AccelerationFactor,
            _config.SteeringFactor,
            trackCollision);
        
        _vehicle = new Vehicle(new NeuroEvolutionPlayer(neuroConfig, 1, 1), carSimulator, vehicleView);

        Store(neuroConfig);
        
        _movementManager.AddVehicle(_vehicle);
        
        Factory.CreateTrackView(_trackModel);
    }

    private NeuroEvolutionConfig NeuroEvolutionConfigChooser(TrackCollision trackCollision)
    {
        if (_neuroConfigJson != null)
        {
            return LoadConfig(_neuroConfigJson);
        }

        return RunGeneticAlgorithm(trackCollision);
    }

    private NeuroEvolutionConfig RunGeneticAlgorithm(TrackCollision trackCollision)
    {
        var evolver = new GeneticEvolver(_trackModel,
            () => new CarSimulator(_config.Mass, _config.Friction, _config.AccelerationFactor,
                _config.SteeringFactor,
                trackCollision));


        var bestConfig = evolver.Run();
        return bestConfig;
    }

    private NeuroEvolutionConfig LoadConfig(TextAsset neuroConfigJson)
    {
        return JsonConvert.DeserializeObject<NeuroEvolutionConfig>(neuroConfigJson.text);
    }

    private void Store(NeuroEvolutionConfig bestConfig)
    {
        var weightsJson = JsonConvert.SerializeObject(bestConfig);
        File.WriteAllText("Assets/Data/Genetic" + DateTime.Now.ToFileTime() +".json" , weightsJson);
    }
}