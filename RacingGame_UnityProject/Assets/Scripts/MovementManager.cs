﻿using System;
using System.Collections.Generic;
using System.Linq;
using AiSolution.NeuroEvolution.Simulation;
using AiSolution.Players;
using CarPhysics;
using Raycasting;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    private List<Vehicle> _vehicles = new List<Vehicle>();
    private static readonly int[] Angles = {-90, -45, 0, 45, 90};
    
    private List<Line> _walls;
    

    public void Inject(TrackModel trackModel)
    {
        _walls = trackModel.GetAllWalls();
    }
    
    
    private void Update()
    {
        Tick();
    }

    private void Tick()
    {
        foreach (var vehicle in _vehicles)
        {
            var vehicleSimulator = vehicle.VehicleSimulator;
            var vehicleView = vehicle.VehicleView;

            var list = Angles.Select(angle => 
                CollisionHelper.GetCollisionVector(_walls, vehicle.VehicleSimulator.Position,
                    vehicle.VehicleSimulator.Rotation, angle)).ToList();

            var perception = new Perception
            {
                RaycastLeft = list[0].magnitude / 10,
                RaycastFrontLeft = list[1].magnitude / 10,
                RaycastFront = list[2].magnitude / 10,
                RaycastFrontRight = list[3].magnitude / 10,
                RaycastRight = list[4].magnitude / 10,
                
                Velocity = vehicleSimulator.Velocity / 10
            };

            var nextMove = vehicle.Driver.NextMove(perception);
            
            vehicleSimulator.ProgressByDeltaTime((float)nextMove.Horizontal, (float)nextMove.Vertical, (float) Simulator.SimulationSpeed);
            vehicleView.UpdatePositionAndRotation(vehicleSimulator.Position, vehicleSimulator.Rotation);
        }
    }

    public void AddVehicle(Vehicle vehicle)
    {
        _vehicles.Add(vehicle);
    }

}