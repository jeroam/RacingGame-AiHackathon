﻿using System;
using System.Collections;
using System.Collections.Generic;
using AiSolution.Neural_Networks;
using AiSolution.NeuroEvolution.Simulation;
using AiSolution.Players;
using CarPhysics;
using Configs;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class VehicleSpawner
{
    [SerializeField] private VehicleSpawnerConfig _vehicleSpawnerConfig;
    [SerializeField] private MovementManager _movementManager;
    [SerializeField] private readonly float _delay;

    public TrackCollision TrackCollision { private get; set; }

    private WaitForSeconds _wait;
    private List<IPlayer> _players = new List<IPlayer>();

    public VehicleSpawner(VehicleSpawnerConfig vehicleSpawnerConfig, float delay, MovementManager movementManager)
    {
        _vehicleSpawnerConfig = vehicleSpawnerConfig;
        _delay = delay;
        _movementManager = movementManager;
    }

    public IEnumerator StartSpawning()
    {
        _wait = new WaitForSeconds(_delay);
        var bundles = _vehicleSpawnerConfig.VehicleBundleSettings;
        foreach (var bundle in bundles)
        {
            for (var i = 0; i < bundle.AmountToSpawn; i++)
            {
                IPlayer player = null;

                switch (bundle.InputScheme)
                {
                    case InputScheme.Player:
                        player = new HumanPlayer();
                        break;
                    case InputScheme.Random:
                        var randomHorizontal = new Vector2(bundle.RandomPlayerConfig.HorizontalMin,
                            bundle.RandomPlayerConfig.HorizontalMax);
                        var randomVertical = new Vector2(bundle.RandomPlayerConfig.VerticalMin,
                            bundle.RandomPlayerConfig.VerticalMax);
                        player = new RandomPlayer(randomHorizontal, randomVertical, bundle.ChangeDirectionProbability);
                        break;
                    case InputScheme.Neuronal:
                        var simulator = new Simulator(TrackCollision.TrackModel, 2000, null);
                        player = CreateNeuralPlayer();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                Spawn(bundle.VehicleSimulatorConfig, player);
                _players.Add(player);
                yield return _wait;
            }
        }
    }

    private Simulator.Evaluation EvaluatePlayer(Simulator simulator, IPlayer player, VehicleSpawnerConfig.VehicleBundleSetting bundle)
    {
        return simulator.Evaluate(((NeuralNetworkPlayer) player).Config,
            new CarSimulator(bundle.VehicleSimulatorConfig.Mass,
                bundle.VehicleSimulatorConfig.Friction,
                bundle.VehicleSimulatorConfig.AccelerationFactor,
                bundle.VehicleSimulatorConfig.SteeringFactor, TrackCollision));
    }

    private NeuralNetworkPlayer CreateNeuralPlayer()
    {
        return new NeuralNetworkPlayer(new NeuralNetworkPlayerConfig
        {
            HiddenLayers = 0,
            NeuronsPerLayer = 3,
            MaxVerticalOutput = 1,
            MaxHorizontalOutput = 1,
            LearningRate = 2
        });
    }

    private void Spawn(VehicleSimulatorConfig simConfig, IPlayer player)
    {
        var carSimulator = new CarSimulator(simConfig.Mass, simConfig.Friction, simConfig.AccelerationFactor,
            simConfig.SteeringFactor, TrackCollision);



        var vehicleV = GameObject.Instantiate(_vehicleSpawnerConfig.VehiclePrefab, TrackCollision.StartPosition(), Random.rotation);
        vehicleV.GetComponent<SpriteRenderer>().sprite = simConfig.Sprite;
        var newVehicle = new Vehicle(player, carSimulator, vehicleV);
        
        _movementManager.AddVehicle(newVehicle);
    }

    public void Dispose()
    {
        _players.ForEach(player => player.Dispose());
    }
}