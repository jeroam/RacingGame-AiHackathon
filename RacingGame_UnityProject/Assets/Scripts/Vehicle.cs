﻿using AiSolution.Players;
using CarPhysics;
using UnityEngine;

public class Vehicle
{
    public IPlayer Driver;
    public CarSimulator VehicleSimulator;
    public VehicleView VehicleView;

    public Vehicle(IPlayer driver, CarSimulator vehicleSimulator, VehicleView vehicleView)
    {
        Driver = driver;
        VehicleSimulator = vehicleSimulator;
        VehicleView = vehicleView;
    }
}