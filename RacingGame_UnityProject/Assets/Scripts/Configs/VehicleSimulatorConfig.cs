﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "UnityCarSimulatorConfig", menuName = "ScriptableObjects/CarConfig")]
    public class VehicleSimulatorConfig : ScriptableObject
    {
        public Sprite Sprite;
        public float Mass;
        public float Friction;
        public float AccelerationFactor;
        public float SteeringFactor;
    }
}