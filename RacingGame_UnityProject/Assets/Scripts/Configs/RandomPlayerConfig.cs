﻿using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "RandomPlayerConfig", menuName = "ScriptableObjects/RandomPlayerConfig")]
    public class RandomPlayerConfig : ScriptableObject
    {
        [Range(-1f, 1f)] public float HorizontalMin;
        [Range(-1f, 1f)] public float HorizontalMax;
        [Range(-1f, 1f)] public float VerticalMin;
        [Range(-1f, 1f)] public float VerticalMax;
    }
}