﻿using System;
using UnityEngine;

namespace Configs
{
    public enum InputScheme
    {
        Player = 0,
        Random = 1,
        Neuronal = 2
    }

    [CreateAssetMenu(fileName = "UnityCarSpawnerConfig", menuName = "ScriptableObjects/CarSpawnerConfig")]
    public class VehicleSpawnerConfig : ScriptableObject
    {
        public VehicleView VehiclePrefab;
        public GameObject PointPrefab;

        [Serializable]
        public class VehicleBundleSetting
        {
            public InputScheme InputScheme;
            public RandomPlayerConfig RandomPlayerConfig;
            public VehicleSimulatorConfig VehicleSimulatorConfig;
            public int AmountToSpawn;
            [Range(0f, 1f)]public float ChangeDirectionProbability = 1f;
        }

        public VehicleBundleSetting[] VehicleBundleSettings;
    }
}