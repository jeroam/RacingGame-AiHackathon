﻿using CarPhysics;
using UnityEngine;

public class VehicleView : MonoBehaviour
{

    public void UpdatePositionAndRotation(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
    }
}