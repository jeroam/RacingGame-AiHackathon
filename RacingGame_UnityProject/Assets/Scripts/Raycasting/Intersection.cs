﻿namespace Raycasting
{
    public class Intersection
    {
        public bool Exists { get; }
        public float RayMagnitude { get; }

        public Intersection(bool exists, float rayMagnitude)
        {
            Exists = exists;
            RayMagnitude = rayMagnitude;
        }

        public override string ToString()
        {
            return "Intersection exists: " + Exists + " RayMagnitude: " + RayMagnitude;
        }
    }
}