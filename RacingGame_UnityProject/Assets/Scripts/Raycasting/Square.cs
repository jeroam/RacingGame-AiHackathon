﻿using System.Collections.Generic;
using UnityEngine;

namespace Raycasting
{
    public class Square
    {
        private readonly float _scale;
        private readonly Vector2 _worldPosBottomLeft;
        
        private readonly Line[] _sides;

        public Square(float scale, Vector2 worldPosBottomLeft)
        {
            _scale = scale;
            _worldPosBottomLeft = worldPosBottomLeft;
            _sides =  new[]
            {
                new Line(_worldPosBottomLeft, _worldPosBottomLeft + new Vector2(_scale, 0)), // bottom
                new Line(_worldPosBottomLeft, _worldPosBottomLeft + new Vector2(0, _scale)), // left
                new Line(_worldPosBottomLeft + new Vector2(_scale, _scale), _worldPosBottomLeft + new Vector2(_scale, 0)), // right
                new Line(_worldPosBottomLeft + new Vector2(_scale, _scale), _worldPosBottomLeft + new Vector2(0, _scale)), // top
            };
        }

        public IEnumerable<Line> GetSides()
        {
            return _sides;
        }
    }
}