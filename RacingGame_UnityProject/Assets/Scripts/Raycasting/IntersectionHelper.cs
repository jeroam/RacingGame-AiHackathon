﻿using UnityEngine;

namespace Raycasting
{
    public static class IntersectionHelper
    {
        public static Intersection CheckIntersection(Line rayLine, Line side)
        {
            var intersectionPoint = Vector2.zero;
			
            bool hasCollided = LineSegmentsIntersect(
                rayLine.Origin, rayLine.End,
                side.Origin, side.End, 
                out intersectionPoint);

            var carToIntersection = intersectionPoint - rayLine.Origin;
            return new Intersection(hasCollided, carToIntersection.magnitude);
        }
        
        /// <remarks>
        /// Sourced from: https://www.codeproject.com/Tips/862988/Find-the-Intersection-Point-of-Two-Line-Segments
        /// </remarks>
        private static bool LineSegmentsIntersect(Vector2 p, Vector2 p2, Vector2 q, Vector2 q2,
            out Vector2 intersection, bool considerCollinearOverlapAsIntersect = false)
        {
            intersection = Vector2.zero;

            var r = p2 - p;
            var s = q2 - q;
            var rxs = r.Cross(s);
            var qpxr = (q - p).Cross(r);

            // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
            if (rxs.IsZero() && qpxr.IsZero())
            {
                // 1. If either  0 <= (q - p) * r <= r * r or 0 <= (p - q) * s <= * s
                // then the two lines are overlapping,
                if (considerCollinearOverlapAsIntersect)
                {
                    if ((0 <= (q - p).Dot(r) && (q - p).Dot(r) <= r.Dot(r) 
                     || (0 <= (p - q).Dot(s) && (p - q).Dot(s) <= s.Dot(s))))
                        return true;
                }

                // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
                // then the two lines are collinear but disjoint.
                // No need to implement this expression, as it follows from the expression above.
                return false;
            }

            // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
            if (rxs.IsZero() && !qpxr.IsZero())
                return false;

            var t = (q - p).Cross(s) / rxs;

            var u = (q - p).Cross(r) / rxs;

            // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
            // the two line segments meet at the point p + t r = q + u s.
            if (!rxs.IsZero() && (0 <= t && t <= 1) && (0 <= u && u <= 1))
            {
                // We can calculate the intersection point using either t or u.
                intersection = p + t * r;

                // An intersection was found.
                return true;
            }

            // 5. Otherwise, the two line segments are not parallel but do not intersect.
            return false;
        }
    }
}