﻿using UnityEngine;

namespace Raycasting
{
    public class Line
    {
        public Vector2 Origin;
        public Vector2 End;
			
        public Line(Vector2 origin, Vector2 end)
        {
            Origin = origin;
            End = end;
        }

        public override string ToString() => $"Origin: {Origin} End: {End}";
    }
}