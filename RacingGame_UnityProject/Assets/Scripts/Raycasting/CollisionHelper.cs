﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Raycasting
{
    public static class CollisionHelper
    {
        private const int RayDistance = 1000;

        /// <summary>
        /// Given a set of collision boundaries and the position and rotation of an object,
        /// calculates a vector from the object at the given relative angle to the nearest collision boundary.
        /// </summary>
        /// <param name="collisionBounds">A collection of boundaries to test against.</param>
        /// <param name="position">Current position of the object.</param>
        /// <param name="rotation">Current rotation of the object.</param>
        /// <param name="angle">Relative angle of the vector.</param>
        /// <returns></returns>
        public static Vector2 GetCollisionVector(IEnumerable<Line> collisionBounds, 
            Vector3 position, Quaternion rotation, float angle)
            => RotatedAngleToVector(rotation, angle) * 
               GetCollisionMagnitude(position, collisionBounds, RotatedAngleToRay(position, rotation, angle));
		
        private static float GetCollisionMagnitude(Vector3 position, IEnumerable<Line> collisionBounds, Vector3 ray)
        {
            var rayLine = new Line(position, ray);

            // Checks all sides of cube, 1 and only 1 side should ever be hit.at the given position and rotation
            
            var possibleIntersections = collisionBounds.Select(bound 
                => IntersectionHelper.CheckIntersection(rayLine, bound));

            return possibleIntersections
                .Where(possibleIntersection => possibleIntersection.Exists)
                .Min(intersect => intersect.RayMagnitude);
        }

        private static Vector3 RotatedAngleToVector(Quaternion rotation, float angle)
            => rotation * Quaternion.Euler(0, 0, -angle) * Vector3.up;
		
        private static Vector2 RotatedAngleToRay(Vector3 position, Quaternion rotation, float angle)
            => position + RotatedAngleToVector(rotation, angle) * RayDistance;
    }
    
                   
//                Debug.Log("Ray: "  + rayLine 
//                + Environment.NewLine + "Side: " + bound
//                + Environment.NewLine + "Intersection: " + possibleIntersection);
}