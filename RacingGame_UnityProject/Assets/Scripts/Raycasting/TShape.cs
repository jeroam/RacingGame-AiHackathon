﻿using System.Collections.Generic;
using UnityEngine;

namespace Raycasting
{
    public class TShape
    {
        private readonly Line[] _sides;

        public IEnumerable<Line> GetSides()
        {
            return _sides;
        }
        
        public TShape()
        {
            _sides =  new[]
            {
                new Line(Vector2.zero, Vector2.right), // bottom
                new Line(Vector2.zero, Vector2.up), // left
                new Line(Vector2.one, Vector2.right), // right
                
                new Line(Vector2.up , Vector2.up + Vector2.left), // left part t 
                new Line(Vector2.up + Vector2.left , Vector2.up * 2 + Vector2.left), // left part t 
                new Line(Vector2.up * 2 + Vector2.left , Vector2.up * 2), // left part t
                
                new Line(Vector2.up * 2, Vector2.up * 2 + Vector2.right ), // t middle 
                
                new Line(Vector2.up * 2 + Vector2.right, Vector2.one * 2), // right t 
                new Line(Vector2.one * 2, Vector2.one * 2 + Vector2.down), // right t 
                new Line(Vector2.one * 2 + Vector2.down, Vector2.one), // right t 
            };
        }
    }

    public class BigSquare
    {
        private readonly Line[] _sides;

        public IEnumerable<Line> GetSides()
        {
            return _sides;
        }
        
        public BigSquare()
        {
            _sides =  new[]
            {
                new Line(Vector2.zero, Vector2.right * 4), // bottom
                new Line(Vector2.zero, Vector2.up * 4), // left
                new Line(Vector2.one * 4, Vector2.right * 4), // right
                new Line(Vector2.one * 4, Vector2.up * 4), // top
            };

            foreach (var side in _sides)
            {
                Debug.Log(side);
            }
        }
    }
}