﻿using UnityEngine;

namespace Raycasting
{
    public static class Vector2Extensions
    {
        public static float Cross(this Vector2 v1, Vector2 v2)
        {
            return v1.x * v2.y - v1.y * v2.x;
        }
        
        public static float Dot(this Vector2 v1, Vector2 v2)
        {
            return v1.x * v2.x + v1.y * v2.y;
        }
        
        private const double Epsilon = 1e-10;

        public static bool IsZero(this float d)
        {
            return Mathf.Abs(d) < Epsilon;
        }
    }
}