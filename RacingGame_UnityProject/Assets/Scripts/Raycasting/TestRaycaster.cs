﻿using UnityEditor;
using UnityEngine;

namespace Raycasting
{
	public class TestRaycaster : MonoBehaviour
	{
		private static readonly int[] Angles = new[] {0, 45, -45, 90, -90};

		private static Square _testSquare = new Square(1, Vector2.zero);

		
		private void OnDrawGizmos()
		{
			Gizmos.color = Color.red;

			foreach (var angle in Angles)
			{
				Gizmos.DrawRay(transform.position, 
					CollisionHelper.GetCollisionVector(
						 new TShape().GetSides(),
						transform.position, transform.rotation, angle));
			}
		}

		
		
//use for performance testing
		
//		private void Update()
//		{
//			for (int i = 0; i < 100; i++)
//			{
//				foreach (var angle in Angles)
//				{
//					CollisionHelper.GetCollisionVector(
//							_testSquare.GetSides(),
//							transform.position, transform.rotation, angle);
//				}
//			}
//		}
	}
}

	
