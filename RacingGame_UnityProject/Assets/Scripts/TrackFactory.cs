﻿using CarPhysics;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class TrackFactory : SerializedMonoBehaviour
{
    public GameObject RoadTilePrefab;

    [OdinSerialize]
    public int[,] TrackData;

    // Use this for initialization
    public void CreateTrackView(TrackModel trackModel)
    {
        for (var trackIndex = 1; trackIndex <= trackModel.MaxTileIndex; trackIndex++)
        {
            var tile = SpawnTrackTile(trackModel.GetTilePosition(trackIndex));
            if (trackIndex == 1)
            {
                var renderer = tile.GetComponent<SpriteRenderer>();
                renderer.color = Color.cyan;
            }

            if (trackIndex == trackModel.MaxTileIndex)
            {
                var renderer = tile.GetComponent<SpriteRenderer>();
                renderer.color = Color.green;
            }
        }
	}

    private GameObject SpawnTrackTile(Vector2 position)
    {
        return Instantiate(RoadTilePrefab, position, Quaternion.identity);
    }
}
