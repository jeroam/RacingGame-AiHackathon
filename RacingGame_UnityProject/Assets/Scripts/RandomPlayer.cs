﻿using AiSolution.Players;
using UnityEngine;

public class RandomPlayer : IPlayer
{
    private readonly Vector2 _horizontalRandomRange;
    private readonly Vector2 _verticalRandomRange;
    private readonly float _changeDirectionProbability;

    private Move _lastMove;

    public RandomPlayer(Vector2 horizontalRandomRange, Vector2 verticalRandomRange, float changeDirectionProbability)
    {
        _horizontalRandomRange = horizontalRandomRange;
        _verticalRandomRange = verticalRandomRange;
        _changeDirectionProbability = changeDirectionProbability;
    }

    public Move NextMove(Perception perception)
    {
        var move = new Move();
        if (Random.Range(0f, 1f) > _changeDirectionProbability)
        {
            move = _lastMove;
        }
        else
        {
            move = new Move
            {
                Horizontal = Random.Range(_horizontalRandomRange[0], _horizontalRandomRange[1]),
                Vertical = Random.Range(_verticalRandomRange[0], _verticalRandomRange[1])
            };
            _lastMove = move;

        }

        return move;
    }

    public void Dispose()
    {
            
    }
}