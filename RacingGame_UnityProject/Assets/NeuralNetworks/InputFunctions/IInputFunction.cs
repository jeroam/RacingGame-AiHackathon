﻿/*
 * Author: Nikola Živković
 * Website: rubikscode.net
 * Year: 2018
 */

using System.Collections.Generic;
using NeuralNetworks.Synapses;

namespace NeuralNetworks.InputFunctions
{
    /// <summary>
    /// Interface for Input Functions.
    /// </summary>
    public interface IInputFunction
    {
        double CalculateInput(List<ISynapse> inputs);
    }
}
