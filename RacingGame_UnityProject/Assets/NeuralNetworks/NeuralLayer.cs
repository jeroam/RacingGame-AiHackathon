﻿/*
 * Author: Nikola Živković
 * Website: rubikscode.net
 * Year: 2018
 */

using System.Collections.Generic;
using System.Linq;
using NeuralNetworks.Neuron;

namespace NeuralNetworks
{
    /// <summary>
    /// Implementation of the single layer in Artificial Neural Network.
    /// </summary>
    public class NeuralLayer
    {
        public List<INeuron> Neurons;

        public NeuralLayer()
        {
            Neurons = new List<INeuron>();
        }

        /// <summary>
        /// Connecting two layers.
        /// </summary>
        public void ConnectLayers(NeuralLayer inputLayer, List<List<double>> weights = null)
        {
            if (weights == null)
            {
                var combos = Neurons.SelectMany(neuron => inputLayer.Neurons, (neuron, input) => new {neuron, input});
                combos.ToList().ForEach(x => x.neuron.AddInputNeuron(x.input));
            }
            else
            {
                for (var input = 0; input < inputLayer.Neurons.Count; input++)
                {
                    for (var neuron = 0; neuron < Neurons.Count; neuron++)
                    {
                        Neurons[neuron].AddInputNeuron(inputLayer.Neurons[input], weights[input][neuron]);
                    }
                }
            }
        }
    }
}
