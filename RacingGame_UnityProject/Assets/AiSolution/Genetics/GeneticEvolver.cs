﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AiSolution.Neural_Networks;
using AiSolution.NeuroEvolution;
using AiSolution.NeuroEvolution.Simulation;
using CarPhysics;
using NeuralNetworks.ActivationFunctions;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace AiSolution.Genetics
{
    public class GeneticEvolver
    {
        private Simulator _simulator;

        private Func<CarSimulator> _carFactory;
        private NeuralNetworkPlayerConfig _networkConfig;
        private readonly WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame();

        public GeneticEvolver(TrackModel trackModel, Func<CarSimulator> carFactory)
        {
            _simulator = new Simulator(trackModel, 1000, null);
            _carFactory = carFactory;
        }

        public NeuroEvolutionConfig Run()
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            const double mutationProbability = 0.6;
            
            const int populationSize = 100;
      
            const int generations = 100;        

            _networkConfig = new NeuralNetworkPlayerConfig
            {
                HiddenLayers = 1,
                NeuronsPerLayer = 3,
                ActivationFunction = new SigmoidActivationFunction(1),
            };
            
            var currentGeneration = new List<Dude>();
            var currentAllTimeBestDude = new Dude();
            
            for (var dude = 0; dude < populationSize; dude++)
            {
                //creates a random set of weights
                var neuralNetwork = new NeuralNetworkPlayer(_networkConfig);
                currentGeneration.Add(new Dude
                {
                    Config =  neuralNetwork.Config,
                    Fitness = 0,
                });
            }

            for (int generation = 0; generation < generations; generation++)
            {
                foreach (var dude in currentGeneration.Skip(1))
                {
                    dude.Config.Mutate(mutationProbability);
                    dude.Fitness = EvaluateFitness(dude.Config);
                }

                var generationsBestDude = currentGeneration.OrderByDescending(dude => dude.Fitness).First();
                
                UpdateBestDude(currentAllTimeBestDude, generationsBestDude);
                Debug.Log("Generation: " + generation
                                         + " Best Dude of Generation: " + generationsBestDude.Fitness
                                         + " All Time Best Dude: " + currentAllTimeBestDude.Fitness);  
                
                //yield return _waitForEndOfFrame;
                foreach (var dude in currentGeneration)
                {
                    dude.Config = generationsBestDude.Config.DeepCopy();
                    dude.Fitness = generationsBestDude.Fitness;
                }

                if (currentAllTimeBestDude.Fitness >= 0.99)
                {
                    return currentAllTimeBestDude.Config;
                }
            }

            return currentAllTimeBestDude.Config;
            // yield return null;
        }

        private static void UpdateBestDude(Dude currentAllTimeBestDue, Dude generationBestDude)
        {
            if (generationBestDude.Fitness > currentAllTimeBestDue.Fitness)
            {
                currentAllTimeBestDue.Config = generationBestDude.Config.DeepCopy();
                currentAllTimeBestDue.Fitness = generationBestDude.Fitness;
            }
        }

        private double EvaluateFitness(NeuroEvolutionConfig config)
        {
            return _simulator.Evaluate(config, _carFactory()).Fitness;
        }
    }

    public class Dude
    {
        public NeuroEvolutionConfig Config;
        public double Fitness;
    }
}