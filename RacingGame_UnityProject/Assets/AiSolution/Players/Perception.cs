﻿using System;

namespace AiSolution.Players
{
    [Serializable]
    public struct Perception
    {
        public double RaycastLeft;
        public double RaycastFrontLeft;
        public double RaycastFront;
        public double RaycastFrontRight;
        public double RaycastRight;

        public double Velocity;

        public double[] ToInputVector()
        {
            return new []
            {
                RaycastLeft,
                RaycastFrontLeft,
                RaycastFront,
                RaycastFrontRight,
                RaycastRight,
                Velocity
            };
        }
    }
}