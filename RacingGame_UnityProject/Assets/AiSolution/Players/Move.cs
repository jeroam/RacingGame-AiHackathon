﻿using System;

namespace AiSolution.Players
{
    [Serializable]
    public struct Move
    {
        public double Horizontal;
        public double Vertical;

        public double[] ToOutputVector()
        {
            return new[]
            {
                Horizontal, 
                Vertical
            };
        }

        public override string ToString()
        {
            return "Horizontal: " + Horizontal + " Vertical: " + Vertical;
        }

        public static Move FromVector(double[] vector)
        {
            return new Move
            {
                Horizontal = vector[0],
                Vertical = vector[1]
            };
        }
    }
}