﻿using System;

namespace AiSolution.Players
{
    public interface IPlayer : IDisposable
    {
        Move NextMove(Perception perception);
    }
}