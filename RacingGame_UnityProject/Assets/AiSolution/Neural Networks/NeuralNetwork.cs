﻿using System.Collections.Generic;
using System.Linq;
using AiSolution.NeuroEvolution;
using NeuralNetworks;
using NeuralNetworks.ActivationFunctions;
using NeuralNetworks.InputFunctions;

namespace AiSolution.Neural_Networks
{
    public class NeuralNetwork
    {
        private readonly IActivationFunction _activationFunction;
        private readonly IInputFunction _inputFunction;
        private readonly SimpleNeuralNetwork _network;
        private readonly NeuralLayerFactory _factory = new NeuralLayerFactory();

        public NeuralNetwork(NeuralNetworkConfig config)
        {
            _activationFunction = config.ActivationFunction ?? new SigmoidActivationFunction(1);
            _inputFunction = config.InputFunction ?? new WeightedSumFunction();

            _network = new SimpleNeuralNetwork(config.InputNeurons, -config.LearningRate);
            AddHiddenLayers(config.HiddenLayers, config.NeuronsPerLayer);
            AddOutputLayer(config.OutputNeurons);
        }

        private void AddHiddenLayers(int layers, int neurons)
        {
            for (var _ = 0; _ < layers; _++)
            {
                _network.AddLayer(_factory.CreateNeuralLayer(neurons, _activationFunction, _inputFunction));
            }
        }

        private void AddOutputLayer(int outputNeurons)
        {
            _network.AddLayer(_factory.CreateNeuralLayer(outputNeurons, _activationFunction, _inputFunction));
        }

        public List<double> FeedForward(double[] input)
        {
            _network.PushInputValues(input);
            return _network.GetOutput();
        }

        public override string ToString()
        {
            return _network.ToString();
        }

        public struct TrainingExample
        {
            public double[] Input;
            public double[] ExpectedOutput;
        }

        public void Train(List<TrainingExample> examples, int numberOfEpochs)
        {
            _network.PushExpectedValues(examples.Select(example => example.ExpectedOutput).ToArray());
            _network.Train(examples.Select(example => example.Input).ToArray(), numberOfEpochs);
        }

        public NeuroEvolutionConfig Config => new NeuroEvolutionConfig
        {
            Weights = _network.GetWeights()
        };
    }
}