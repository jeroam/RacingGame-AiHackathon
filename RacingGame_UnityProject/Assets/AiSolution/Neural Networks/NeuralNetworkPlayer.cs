﻿using System.Collections.Generic;
using System.Linq;
using AiSolution.NeuroEvolution;
using AiSolution.Players;

namespace AiSolution.Neural_Networks
{
    public class NeuralNetworkPlayer : IPlayer
    {
        private readonly NeuralNetwork _network;
        private readonly float _maxVerticalOutput;
        private readonly float _maxHorizontalOutput;
        private readonly Trainer _trainer;

        public NeuralNetworkPlayer(NeuralNetworkPlayerConfig config)
        {
            _network = new NeuralNetwork(new NeuralNetworkConfig
            {
                InputNeurons = 6,
                OutputNeurons = 2,
                HiddenLayers = config.HiddenLayers,
                NeuronsPerLayer = config.NeuronsPerLayer,
                LearningRate = config.LearningRate,
                ActivationFunction = config.ActivationFunction,
                InputFunction = config.InputFunction
            });
            _maxVerticalOutput = config.MaxVerticalOutput;
            _maxHorizontalOutput = config.MaxHorizontalOutput;
            _trainer = new Trainer();
        }

        public Move NextMove(Perception perception)
        {
            var output = _network.FeedForward(perception.ToInputVector());
            //UnityEngine.Debug.Log(JsonUtility.ToJson(perception));
            //UnityEngine.Debug.LogFormat("{0}, {1}", output[0], output[1]);
            var result = new Move
            {
                Horizontal = ScaleHorizontal(output[0]),
                Vertical = output[1]
            };
            /*_trainer.StoreTrainingExample("perceptionReal.txt", new TrainingExample
            {
                Perception = perception,
                ExpectedMove = result
            });*/
            return result;
        }

        private double ScaleHorizontal(double value)
        {
            return (value * 2) - 1;
        }

        private static double ScaleUp(double value, float max)
        {
            return value * 2 * max - max;
        }

        public override string ToString()
        {
            return _network.ToString();
        }

        public void Dispose()
        {
            _trainer.Dispose();
        }

        public void Train(IEnumerable<TrainingExample> examples, int numberOfEpochs)
        {
            _network.Train(examples.Select(example => new NeuralNetwork.TrainingExample
            {
                Input = example.Perception.ToInputVector(),
                ExpectedOutput = example.ExpectedMove.ToOutputVector()
            }).ToList(), numberOfEpochs);
        }

        public NeuroEvolutionConfig Config => _network.Config;
    }
}