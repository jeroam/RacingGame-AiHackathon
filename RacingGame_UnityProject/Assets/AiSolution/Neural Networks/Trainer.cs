﻿using System;
using System.Collections.Generic;
using System.IO;
using Sirenix.Utilities;
using UnityEngine;

namespace AiSolution.Neural_Networks
{
    public class Trainer : IDisposable
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public struct TrainingExampleDataSet
        {
            public List<TrainingExample> Examples;
        }

        private readonly Dictionary<string, List<TrainingExample>> _examplesToStore = new Dictionary<string, List<TrainingExample>>();

        public void StoreTrainingExample(string fileName, TrainingExample example)
        {
            GetExamples(fileName).Add(example);
            Debug.Log("Storing Training Example to " + Application.persistentDataPath + "/" + fileName);
        }

        private List<TrainingExample> GetExamples(string fileName)
        {
            if (_examplesToStore.ContainsKey(fileName))
                return _examplesToStore[fileName];

            _examplesToStore[fileName] = new List<TrainingExample>();
            return _examplesToStore[fileName];
        }

        public List<TrainingExample> LoadTrainingExamples(string filename)
        {
            using (var file = new StreamReader(Application.persistentDataPath + "/" + filename))
                return JsonUtility.FromJson<TrainingExampleDataSet>(file.ReadToEnd()).Examples;
        }

        public void Dispose()
        {
            _examplesToStore.ForEach(pair =>
            {
                using (var file = File.CreateText(Application.persistentDataPath + "/" + pair.Key)) 
                    file.Write(JsonUtility.ToJson(new TrainingExampleDataSet {Examples = pair.Value}));
            });
        }
    }
}