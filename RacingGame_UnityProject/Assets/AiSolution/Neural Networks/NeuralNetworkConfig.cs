﻿using NeuralNetworks.ActivationFunctions;
using NeuralNetworks.InputFunctions;

namespace AiSolution.Neural_Networks
{
    public struct NeuralNetworkConfig
    {
        public int InputNeurons;
        public int OutputNeurons;
        public int HiddenLayers;
        public int NeuronsPerLayer;
        public double LearningRate;

        public IActivationFunction ActivationFunction; // optional
        public IInputFunction InputFunction; // optional
    }
}