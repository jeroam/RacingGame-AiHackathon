﻿using NeuralNetworks.ActivationFunctions;
using NeuralNetworks.InputFunctions;

namespace AiSolution.Neural_Networks
{
    public struct NeuralNetworkPlayerConfig
    {
        public float MaxVerticalOutput;
        public float MaxHorizontalOutput;

        public int HiddenLayers;
        public int NeuronsPerLayer;
            
        public double LearningRate; 

        public IActivationFunction ActivationFunction; // optional
        public IInputFunction InputFunction; // optional
    }
}