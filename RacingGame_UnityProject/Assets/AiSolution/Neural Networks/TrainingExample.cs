﻿using System;
using AiSolution.Players;

namespace AiSolution.Neural_Networks
{
    [Serializable]
    public struct TrainingExample
    {
        public Perception Perception;
        public Move ExpectedMove;
    }
}