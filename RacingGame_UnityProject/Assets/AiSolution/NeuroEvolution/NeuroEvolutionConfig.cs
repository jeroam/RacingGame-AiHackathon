﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace AiSolution.NeuroEvolution
{
    [Serializable]
    public struct NeuroEvolutionConfig
    {
        public List<List<List<double>>> Weights;

        public NeuroEvolutionConfig DeepCopy()
        {
            return new NeuroEvolutionConfig
            {
                Weights = Weights.Select(layer => layer.Select(neuron => neuron.Select(connection => connection).ToList()).ToList()).ToList()
            };
        }

        public void Mutate(double mutationPercentage)
        {
            foreach (var layer in Weights)
            {
                foreach (var neuron in layer)
                {
                    for (var i = 0; i < neuron.Count; i++)
                    {
                        if (Random.value < mutationPercentage)
                        {
                            var connection = neuron[i];
                            var randomizedConnection = connection + Random.Range(-1f, 1f);
                            neuron[i] =  randomizedConnection;
                        }
                    }
                }
            }
        }
    }
}