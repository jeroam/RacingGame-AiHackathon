﻿using System.Collections.Generic;
using System.Linq;
using AiSolution.Neural_Networks;
using AiSolution.Players;
using CarPhysics;
using Raycasting;
using UnityEngine;

namespace AiSolution.NeuroEvolution.Simulation
{
    public class Simulator
    {
        public struct Evaluation
        {
            public double Fitness;
            public List<Vector3> Way;
        }

        private readonly TrackModel _track;
        private readonly int _stepAmount;
        private readonly double _deltaTime;
        private readonly List<Line> _walls;
        private readonly Trainer _trainer = new Trainer();

        private static readonly int[] Angles = {-90, -45, 0, 45, 90};

        public static double SimulationSpeed = 0.03;

        public Simulator(TrackModel track, int stepAmount, double? deltaTime)
        {
            _track = track;
            _stepAmount = stepAmount;
            _deltaTime = deltaTime ?? SimulationSpeed;
            _walls = track.GetAllWalls();
        }
 
        public Evaluation Evaluate(NeuroEvolutionConfig config, CarSimulator car)
        {
            var latestPosition = _track.StartPosition();
            var player = new NeuroEvolutionPlayer(config, 1, 1);
            var way = new List<Vector3>();
            for (var step = 0; step < _stepAmount && !car.Crashed; step++)
            {
                latestPosition = car.Position;
                way.Add(latestPosition);
                SimulateStep(player, car);
            }
            return new Evaluation
            {
                Fitness = _track.GetTrackProgressAtPosition(latestPosition),
                Way = way
            };
        }

        private void SimulateStep(IPlayer player, CarSimulator car)
        {
           var list = Angles.Select(angle => 
                CollisionHelper.GetCollisionVector(_walls, car.Position,
                    car.Rotation, angle)).ToList();

            var perception = new Perception
            {
                RaycastLeft = list[0].magnitude / 10,
                RaycastFrontLeft = list[1].magnitude / 10,
                RaycastFront = list[2].magnitude / 10,
                RaycastFrontRight = list[3].magnitude / 10,
                RaycastRight = list[4].magnitude / 10,
                
                Velocity = car.Velocity / 10
            };
            var nextMove = player.NextMove(perception);
            car.ProgressByDeltaTime((float) nextMove.Horizontal, (float) nextMove.Vertical, (float) _deltaTime);
        }
    }
}