﻿using AiSolution.Players;
using NeuralNetworks;
using NeuralNetworks.ActivationFunctions;
using NeuralNetworks.InputFunctions;

namespace AiSolution.NeuroEvolution
{
    public class NeuroEvolutionPlayer : IPlayer
    {
        private readonly float _maxHorizontalOutput;
        private readonly float _maxVerticalOutput;
        private readonly SimpleNeuralNetwork _network;
        private readonly NeuralLayerFactory _factory = new NeuralLayerFactory();

        public NeuroEvolutionPlayer(NeuroEvolutionConfig config, float maxHorizontalOutput, float maxVerticalOutput)
        {
            _maxHorizontalOutput = maxHorizontalOutput;
            _maxVerticalOutput = maxVerticalOutput;
            _network = CreateNetwork(config);
        }

        private SimpleNeuralNetwork CreateNetwork(NeuroEvolutionConfig config)
        {
            var result = new SimpleNeuralNetwork(config.Weights[0].Count);
            for (var index = 0; index < config.Weights.Count - 1; index++)
            {
                var layer = config.Weights[index];
                result.AddLayer(_factory.CreateNeuralLayer(config.Weights[index+1].Count, new SigmoidActivationFunction(1), new WeightedSumFunction()), layer);    
            }
            return result;
        }

        public Move NextMove(Perception perception)
        {
            _network.PushInputValues(perception.ToInputVector());
            var output = _network.GetOutput();
            return new Move
            {
                Horizontal = ScaleHorizontal(output[0]),
                Vertical = output[1]
            };
        }

        private double ScaleHorizontal(double value)
        {
            return (value * 2) - 1;
        }

        private static double ScaleUp(double value, float max)
        {
            return value * 2 * max - max;
        }

        public NeuroEvolutionConfig Configuration => new NeuroEvolutionConfig
        {
            Weights = _network.GetWeights()
        };

        public override string ToString()
        {
            return _network.ToString();
        }

        public void Dispose()
        {
            
        }
    }
}