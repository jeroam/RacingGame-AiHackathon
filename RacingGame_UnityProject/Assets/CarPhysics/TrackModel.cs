﻿using System.Collections.Generic;
using System.Linq;
using Raycasting;
using UnityEngine;

namespace CarPhysics
{
    public class TrackModel
    {
        private readonly int[,] _trackData;
        private readonly Dictionary<int, Vector2> _tilePositionsByIndex = new Dictionary<int, Vector2>();
        private readonly float _tileSize;
        private readonly float _trackSize;
        private readonly Vector2[] _offesets = { Vector2.up, Vector2.down, Vector2.left, Vector2.right};
        private readonly int _arraySize = 0;

        public TrackModel(int[,] trackData, float tileSize)
        {
            _tileSize = tileSize;
            _arraySize = trackData.GetLength(0);
            _trackSize = _arraySize * tileSize;
            _trackData = TransposeY(trackData);
            InitializeTilePositions(_trackData);
            
        }

        private void InitializeTilePositions(int[,] trackData)
        {
            var offset = _tileSize * 0.5f;

            for (var x = 0; x < _arraySize; x++)
                for (var y = 0; y < _arraySize; y++)
                {
                    if(trackData[x, y] > 0)
                        _tilePositionsByIndex.Add(trackData[x, y], new Vector2(x * _tileSize + offset, y * _tileSize + offset));
                }
        }

        public float GetTrackProgressAtPosition(Vector2 position)
        {
            var tileCoordinates = GetTileCoordinates(position);
            var tileIndex = _trackData[tileCoordinates.x,tileCoordinates.y];
            var distanceToNextTile = tileIndex == MaxTileIndex ? 0 
                : DistanceToNextTile(position, _tilePositionsByIndex[tileIndex + 1]);
            return (tileIndex + distanceToNextTile) / MaxTileIndex;
        }

        private float DistanceToNextTile(Vector2 position, Vector2 nextTile)
        {
            return 1 - (nextTile - position).magnitude / _tileSize;
        }

        public bool OnTrack(Vector2 position)
        {
            if (IsPositionOutOfBounds(position))
                return false;

            var tileCoordinates = GetTileCoordinates(position);

            return _trackData[tileCoordinates.x, tileCoordinates.y] > 0;
        }

        private bool IsPositionOutOfBounds(Vector2 position)
        {
            return position.x < 0 || position.x >= _trackSize || position.y < 0 || position.y >= _trackSize;
        }

        private Vector2Int GetTileCoordinates(Vector2 position)
        {
            var xIndex = Mathf.FloorToInt(position.x / _tileSize);
            var yIndex = Mathf.FloorToInt(position.y / _tileSize);
            return new Vector2Int(xIndex, yIndex);
        }

        public int MaxTileIndex => _tilePositionsByIndex.Keys.Max();

        public Vector2 GetTilePosition(int index)
        {
            return _tilePositionsByIndex[index];
        }

        public Vector2 StartPosition()
        {
            return GetTilePosition(1);
        }

        private int[,] TransposeY(int[,] trackData)
        {
            var transposedTrack = new int[_arraySize, _arraySize];
            for (var x = 0; x < _arraySize; x++)
                for (var y = 0; y < _arraySize; y++)
                    transposedTrack[x, _arraySize - y - 1] = trackData[x, y];

            return transposedTrack;
        }

        public List<Line> GetAllWalls()
        {
            var listOfLines = new List<Line>();
            for (var x = 0; x < _arraySize; x++)
            {
                for (var y = 0; y < _arraySize; y++)
                {
                    if (_trackData[x, y] > 0)
                    {
                        CheckAllSides( new Vector2Int(x, y), listOfLines);
                    }
                }
            }
            return listOfLines;
        }

        private void CheckAllSides(Vector2Int arrayPosition, List<Line> listOfLines)
        {
            foreach (var offset in _offesets)
            {
                Vector2 neighbourPosition = arrayPosition + offset;
                if (IsInvalidArrayIndex(neighbourPosition))
                {
                    var position = GetTilePosition(_trackData[arrayPosition.x, arrayPosition.y]);
                    listOfLines.Add(GetLineForOffest(position, offset));
                }
                else if (_trackData[(int)neighbourPosition.x, (int)neighbourPosition.y] == 0)
                {
                    var position = GetTilePosition(_trackData[arrayPosition.x, arrayPosition.y]);
                    listOfLines.Add(GetLineForOffest(position, offset));
                }
            }
        }

        private Line GetLineForOffest(Vector2 position, Vector2 offset)
        {
            var vectorone = new Vector2(-1 * offset.y, -1 * offset.x) * _tileSize * 0.5f;
            var vectortwo = new Vector2(1 * offset.y, 1 * offset.x) * _tileSize * 0.5f;
            var newLine = new Line(position + vectorone, position + vectortwo);
            newLine.End += offset * _tileSize * 0.5f;
            newLine.Origin += offset * _tileSize * 0.5f;
            return newLine;
        }

        private bool IsInvalidArrayIndex(Vector2 neighbourPosition)
        {
            return neighbourPosition.x < 0 || neighbourPosition.y < 0 || neighbourPosition.x >= _arraySize ||
                   neighbourPosition.y >= _arraySize;
        }
    }
}