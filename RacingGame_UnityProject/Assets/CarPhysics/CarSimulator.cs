﻿using UnityEngine;

namespace CarPhysics
{
	public class CarSimulator
	{
		private readonly float _mass;
		private readonly float _friction;
		private readonly float _accelerationFactor;
		private readonly float _steeringFactor;
		private readonly TrackCollision _collision;

		private float _velocityMagnitude = 0.01f;
		private float _steeringForceFactor = 0f;
		private Vector3 _velocity;

		private Vector3 _position = Vector3.zero;
		private Quaternion _rotation = Quaternion.identity;

		public Vector3 Position => _position;
		public Quaternion Rotation => _rotation;
	    public float Velocity => _velocityMagnitude;

	    public bool Crashed;
	
		public CarSimulator(float mass, float friction, float accelerationFactor, float steeringFactor, TrackCollision collision)
		{
			_mass = mass;
			_friction = friction;
			_accelerationFactor = accelerationFactor;
			_steeringFactor = steeringFactor;
			_collision = collision;

		    ResetCar();
		}
	
		public void ProgressByDeltaTime(float horizontalInput, float verticalInput, float deltaTime)
		{
    		float acceleration = verticalInput * _accelerationFactor;
			float steeringForce = horizontalInput * _steeringFactor;

			//Steering magnitude assuming momentum conservation
			_steeringForceFactor = steeringForce * deltaTime / (_mass * _velocityMagnitude) *
			                       Mathf.SmoothStep(0, 1, Mathf.Abs(_velocityMagnitude) * 0.2f);

			//Adding the acceleration and friction
			_velocityMagnitude += (acceleration - _friction * _velocityMagnitude) * deltaTime;

			//Steering force is always perpendicular to the direction of the velocity
			//switch coordinates and change one sign ( alternatively multiply with matrix {{0,1},{-1,0}}) to calculate a normal vector (in 2D)
			_velocity += _steeringForceFactor * new Vector3(_velocity.y, -_velocity.x, 0);

			//Normalization to enforce momentum convervation and clip float impressision
			_velocity = Vector3.Normalize(_velocity) ;
        
			_position += _velocity * deltaTime * _velocityMagnitude;

			_rotation = Quaternion.Euler(0, 0,  Angle(_velocity));

			if (_collision.Collided(_position))
			{
			    Crashed = true;
                ResetCar();
            }
        }

        private void ResetCar()
        {
            _position = _collision.StartPosition();

            _velocityMagnitude = 0.01f;
            _velocity = new Vector3(0, _velocityMagnitude, 0);
        }

        private float Angle(Vector3 direction)
		{
			var angularOffset = 0f;
			if (direction.x < 0)
			{
				angularOffset = Mathf.PI;
			}

			return (Mathf.Atan(direction.y / direction.x) - angularOffset)*180/Mathf.PI - 90;
		}
	}
}