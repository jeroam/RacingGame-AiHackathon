﻿using UnityEngine;

namespace CarPhysics
{
    public class TrackCollision
    {
        private readonly TrackModel _trackModel;
        public TrackModel TrackModel => _trackModel;

        //Assumption: track is based around (0,0,0)
        public TrackCollision(TrackModel trackModel)
        {
            _trackModel = trackModel;
        }

        public bool Collided(Vector2 pos)
        {
            return !_trackModel.OnTrack(pos);
        }

        public Vector3 StartPosition()
        {
            return _trackModel.StartPosition();
        }

    }
}